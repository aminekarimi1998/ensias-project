package com.classroom.app1.UI;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.classroom.app1.DataStatut;
import com.classroom.app1.Model.Product;
import com.classroom.app1.R;
import com.classroom.app1.UI.Adapters.ProductAdapter;
import com.classroom.app1.UI.ClickListeners.RecyclerViewClickListenerProduct;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Map;

public class ProductsListActivity extends AppCompatActivity  {

    final FirebaseFirestore db = FirebaseFirestore.getInstance();
    private ArrayList<Product> productsList = new ArrayList<>();
    private String categorieId;
    private ProductAdapter adapter;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products_list);

        categorieId = (String) getIntent().getSerializableExtra("catName");

        FirebaseApp.initializeApp(this);

        recyclerView = findViewById(R.id.products_list_rc);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));

        fetchData();

    }

    private void fetchData() {
        getCategories(new DataStatut.DataStatus() {
            @Override
            public void onSuccess(ArrayList products) {
                if (adapter == null) {
                    adapter = new ProductAdapter(products, ProductsListActivity.this,
                            new RecyclerViewClickListenerProduct() {
                                @Override
                                public void onClick(View view, Product product) {
                                    Intent productPage = new Intent(ProductsListActivity.this, ProductActivity.class);
                                    productPage.putExtra("nomItem", product.getNom());
                                    productPage.putExtra("descItem", product.getDescription());
                                    productPage.putExtra("idItem", product.getId_product());
                                    productPage.putExtra("imgItem", product.getImg());
                                    productPage.putExtra("priceItem", product.getPrice());
                                    startActivity(productPage);
                                }
                            });
                    recyclerView.setAdapter(adapter);
                } else {
                    adapter.getItems().clear();
                    adapter.getItems().addAll(products);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onError(String e) {
                Toast.makeText(ProductsListActivity.this, e, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getCategories(final DataStatut.DataStatus callback) {

        //final ArrayList<Categories> catList = new ArrayList<>();

        db.collection("categories/" + categorieId + "/products")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {

                                //Log.v("dataFireSec",document.getId()+" "+document.get("img"));

                                ArrayList<String> imgs = new ArrayList<>();
                                Map<String, Object> myMap = document.getData();
                                for (Map.Entry<String, Object> entry : myMap.entrySet()) {
                                    if (entry.getKey().equals("img")) {
                                        for (Object s : (ArrayList) entry.getValue()) {
                                            imgs.add((String) s);
                                        }
                                        Log.v("TagImg", entry.getValue().toString());

                                    }
                                }

                                Product product = new Product(document.getId(),
                                        (String) myMap.get("nom"),
                                        (String) myMap.get("description"),
                                        imgs,
                                        (Double) myMap.get("price"));
                                productsList.add(product);
                            }
                            callback.onSuccess(productsList);

                        } else {
                            callback.onError("Error in data");
                            Log.w("error", "Error getting documents.", task.getException());
                        }

                    }
                });
    }


}
