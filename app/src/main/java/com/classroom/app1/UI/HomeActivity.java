package com.classroom.app1.UI;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.classroom.app1.DataStatut;
import com.classroom.app1.Model.Categories;
import com.classroom.app1.Model.Product;
import com.classroom.app1.R;
import com.classroom.app1.UI.Adapters.CategoriesAdapter;
import com.classroom.app1.UI.ClickListeners.RecyclerViewClickListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Map;

public class HomeActivity extends AppCompatActivity {

    private static final String TAG = "data";
    private RecyclerView recyclerView;
    final FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CategoriesAdapter adapter;
    private ArrayList<Categories> catList2 = new ArrayList<>();
    Toolbar mActionBarToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);

        FirebaseApp.initializeApp(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        // Sets the Toolbar to act as the ActionBar for this Activity window.
        // Make sure the toolbar exists in the activity and is not null
        setSupportActionBar(toolbar);
        recyclerView = findViewById(R.id.categories_rc);

        init();
        fetchData();


    }

    private void fetchData() {
        getCategories(new DataStatut.DataStatus() {
            @Override
            public void onSuccess(ArrayList categories) {
                if (adapter == null) {
                    adapter = new CategoriesAdapter(categories, HomeActivity.this,
                            new RecyclerViewClickListener() {
                                @Override
                                public void onClick(View view, Categories categories) {
                                    Toast.makeText(HomeActivity.this, "", Toast.LENGTH_SHORT).show();
                                    Intent productPage = new Intent(HomeActivity.this, ProductsListActivity.class);
                                    productPage.putExtra("catName", categories.getId_cat());
                                    startActivity(productPage);
                                    //overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_out_to_right);

                                }
                            });
                    recyclerView.setAdapter(adapter);
                } else {
                    adapter.getItems().clear();
                    adapter.getItems().addAll(categories);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onError(String e) {
                Toast.makeText(HomeActivity.this, e, Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void getCategories(final DataStatut.DataStatus callback) {

        //final ArrayList<Categories> catList = new ArrayList<>();

        db.collection("categories")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {

                                Map myMap = document.getData();
                                Log.v("dataFire", (String) myMap.get("name"));
                                // List of categories here
                                Categories categorie = new Categories(document.getId(), (String) myMap.get("name"), (String) myMap.get("icon"), (String) myMap.get("color"));
                                //Product product = new Product(document.getData());
                                catList2.add(categorie);
                            }
                            callback.onSuccess(catList2);

                        } else {
                            callback.onError("Error in data");
                            Log.w(TAG, "Error getting documents.", task.getException());
                        }

                    }
                });
    }

    private void getProducts(final DataStatut.DataStatus callback) {

        db.collection("categories/cat2/products")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d("products", document.getId() + " => " + document.getData());
                            }

                        } else {
                            callback.onError("Error in data");
                            Log.w(TAG, "Error getting documents.", task.getException());
                        }

                    }
                });
    }


    private void init() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext(),
                LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
    }


}
