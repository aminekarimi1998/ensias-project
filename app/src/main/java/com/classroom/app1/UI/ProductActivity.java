package com.classroom.app1.UI;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.classroom.app1.R;
import com.smarteist.autoimageslider.DefaultSliderView;
import com.smarteist.autoimageslider.SliderLayout;
import com.smarteist.autoimageslider.SliderView;

import java.util.ArrayList;

public class ProductActivity extends AppCompatActivity {
    String nomItem, descItem, idItem;
    ArrayList imgItem;
    Double priceItem;
    TextView title_item, price_item, description_item;
    ImageView image;
    SliderLayout sliderLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_page);

        nomItem = (String) getIntent().getSerializableExtra("nomItem");
        descItem = (String) getIntent().getSerializableExtra("descItem");
        imgItem = (ArrayList) getIntent().getSerializableExtra("imgItem");
        idItem = (String) getIntent().getSerializableExtra("idItem");
        priceItem = (Double) getIntent().getSerializableExtra("priceItem");

        Log.v("imgsArray", imgItem.get(0).toString());

        //Setting items
        title_item = findViewById(R.id.title_item);
        description_item = findViewById(R.id.description_item);
        description_item = findViewById(R.id.description_item);
        price_item = findViewById(R.id.price_item);
        //image = findViewById(R.id.image);

        sliderLayout = findViewById(R.id.image);
        //sliderLayout.setIndicatorAnimation(SliderLayout.; //set indicator animation by using SliderLayout.Animations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderLayout.setScrollTimeInSec(3); //set scroll delay in seconds :

        title_item.setText(nomItem);
        description_item.setText(descItem);
        price_item.setText("" + priceItem);

        //Log.v("imgfromArray", imgItem.get(1).toString());

        setSliderViews();

    }

    private void setSliderViews() {

        for (int i = 0; i <= 3; i++) {

            DefaultSliderView sliderView = new DefaultSliderView(this);

            switch (i) {
                case 0:
                    sliderView.setImageUrl(imgItem.get(0).toString());
                    break;
                case 1:
                    sliderView.setImageUrl(imgItem.get(1).toString());
                    break;
                case 2:
                    sliderView.setImageUrl("https://images.pexels.com/photos/747964/pexels-photo-747964.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260");
                    break;
                case 3:
                    sliderView.setImageUrl("https://images.pexels.com/photos/929778/pexels-photo-929778.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260");
                    break;
            }

            sliderView.setImageScaleType(ImageView.ScaleType.CENTER_CROP);
            //sliderView.setDescription("setDescription " + (i + 1));
            sliderView.setOnSliderClickListener(new SliderView.OnSliderClickListener() {
                @Override
                public void onSliderClick(SliderView sliderView) {
                    //Toast.makeText(ProductActivity.this, "This is slider " + (finalI + 1), Toast.LENGTH_SHORT).show();
                }
            });

            //at last add this view in your layout :
            sliderLayout.addSliderView(sliderView);
        }
    }
}
