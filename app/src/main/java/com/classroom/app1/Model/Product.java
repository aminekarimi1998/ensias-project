package com.classroom.app1.Model;

import java.util.ArrayList;

public class Product {
    private String id_product;
    private String description;
    private String nom;
    private ArrayList img;
    private String img_product;
    private Double price;

    public Product(String id_product, String nom, String description, ArrayList img, Double price) {
        this.id_product = id_product;
        this.nom = nom;
        this.description = description;
        this.img = img;
        this.price = price;
    }

    public Product(String id_product, String nom, String description, String img_product, Double price) {
        this.id_product = id_product;
        this.nom = nom;
        this.description = description;
        this.img_product = img_product;
        this.price = price;
    }

    public String getId_product() {
        return id_product;
    }

    public String getNom() {
        return nom;
    }

    public void setId_product(String id_product) {
        this.id_product = id_product;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList getImg() {
        return img;
    }

    public Double getPrice() {
        return price;
    }

    public String getImg_product() {
        return img_product;
    }
}
