package com.classroom.app1;

import java.util.ArrayList;

public class DataStatut {
    public interface DataStatus {
        void onSuccess(ArrayList list);

        void onError(String e);
    }
}
